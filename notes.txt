------ Git Repository ------

Git - is a version control system that manages the revisions/changes/updates of our follder/repository

To create a git repository for out projects folders:

1. Open your terminal/git bash on your project, then run command:

$ git init

- git init is short for git initialized. This will create a git repository that will handle the revisions and histories of your project.

2. On your terminal, run command: 

$ git status

- git status will return a message of all the untracked files (changes/revisions) made on our project.

3. Then run:

$ git add .

- git add . - adds all the untracked files on the staging area before saving it on our git repository. 
- staging area (waiting area/polishing are/finalize files)

4. To save the changes:

$ git commit -m "Initial Commit"

- git commit -m is a way of saving changes/revisions have made on our project.
- -m (flag) short for message
- messages should descriptive. It should describe the features of the updates we have made on our projects.